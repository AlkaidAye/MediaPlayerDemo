package com.aye.audioplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.browse.MediaBrowser;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity{
    private TextView tv_name;
    private RecyclerView rv_list;
    private ImageView iv_play;
    List<Music> musicList;
    private MusicAdapter musicAdapter;
    //记录当前正在播放的音乐的位置
    int currentPlayPosition=-1;
    MediaPlayer mediaPlayer;
    //    记录暂停音乐时进度条的位置
    int currentPausePositionInSong = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_name = findViewById(R.id.tv_name);
        rv_list = findViewById(R.id.rv_list);
        iv_play=findViewById(R.id.iv_start);

        musicList = new ArrayList<>();
        mediaPlayer=new MediaPlayer();
        musicAdapter = new MusicAdapter(this, musicList);
        rv_list.setAdapter(musicAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_list.setLayoutManager(linearLayoutManager);
        getPermission();
        musicData();
        getPermission();
        setEventListener();
    }
    private void getPermission() {
        //1.先请求判断是否具有对应权限
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        //根据返回的结果，判断对应的权限是否有。
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
    }

    //设置每一项的点击事件
    private void setEventListener() {
        musicAdapter.setOnItemClickListener(new MusicAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                currentPlayPosition=position;
                //设置底部显示的歌曲
                Music music = musicList.get(position);
                playMusicInMusicBean(music);
            }
        });
    }
    private void playMusicInMusicBean(Music music) {
        tv_name.setText(music.getSong());
        stop();
        //重置多媒体播放器
        mediaPlayer.reset();
        //设置新的路径
        try {
            mediaPlayer.setDataSource(music.getPath());
            play();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    //加载本地存储当中的音乐文件到集合当中
    private void musicData() {
        //1. 获取ContentResolver对象
        ContentResolver contentResolver = getContentResolver();
        //2. 获取本地存储音乐的uri地址
        Uri uri= MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        //3. 开始查询地址
        Cursor cursor = contentResolver.query(uri, null, null, null, null);
        //4. 遍历cursor
        int id=0;
        while (cursor.moveToNext()){
            String song=cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
            String singer=cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            String path=cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            long duration=cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
            SimpleDateFormat sdf=new SimpleDateFormat("mm:ss");
            String time = sdf.format(new Date(duration));
            id++;
            String sId=String.valueOf(id);
            //5. 将一行当中的数据封装到对象当中
            Music music = new Music(sId, song, singer, time,path);
            musicList.add(music);
        }
        //数据源变化，提示适配器更新
        musicAdapter.notifyDataSetChanged();

    }

    //点击播放
    public void play()  {
        if (mediaPlayer!=null&&!mediaPlayer.isPlaying()) {
            if (currentPausePositionInSong == 0) {
                try {
                    mediaPlayer.prepare();
                    mediaPlayer.start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
//                从暂停到播放
                mediaPlayer.seekTo(currentPausePositionInSong);
                mediaPlayer.start();
            }
            iv_play.setImageResource(R.drawable.pause);
        }
    }
    private void stop() {
        //停止播放
        if (mediaPlayer!=null){
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
            mediaPlayer.stop();
            currentPausePositionInSong=0;
            iv_play.setImageResource(R.drawable.play);
        }
    }

    private void pause() {
        //暂停播放
        if (mediaPlayer!=null&&mediaPlayer.isPlaying()){
            currentPausePositionInSong=mediaPlayer.getCurrentPosition();
            mediaPlayer.pause();
            iv_play.setImageResource(R.drawable.play);
        }
    }


    public void nextMusic(View view) {
        if (currentPlayPosition ==musicList.size()-1) {
            Toast.makeText(this,"已经是最后一首了，没有下一曲！",Toast.LENGTH_SHORT).show();
            return;
        }
        currentPlayPosition = currentPlayPosition+1;
        Music music1 = musicList.get(currentPlayPosition);
        playMusicInMusicBean(music1);
    }

    public void playMusic(View view) {
        if (currentPlayPosition==-1){
            //并没有选中要播放的音乐
            Toast.makeText(this,"未选择音乐",Toast.LENGTH_SHORT).show();
            return;
        }
        if (mediaPlayer.isPlaying()){
            //处于播放状态，需要暂停
            pause();
        }else {
            //没有播放音乐，开始播放
            play();
        }
    }

    public void lastMusic(View view) {
        if (currentPlayPosition ==0) {
            Toast.makeText(this,"已经是第一首了，没有上一曲！",Toast.LENGTH_SHORT).show();
            return;
        }
        currentPlayPosition = currentPlayPosition-1;
        Music music = musicList.get(currentPlayPosition);
        playMusicInMusicBean(music);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stop();
    }
}